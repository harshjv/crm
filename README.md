# CRM

> Node version requirement: ~4.4.0

## Demo

https://dummy-crm.herokuapp.com


## Build and run locally

    git clone https://github.com/harshjv/almabasecrm.git
    cd almabasecrm
    npm install && npm run dev

Then visit http://localhost:6060


## Enable browser logs

Run this in the developer console;

    localStorage.debug = 'almabase*'
