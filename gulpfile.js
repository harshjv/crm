var debug = require('debug')('almabase:gulpfile')

var gulp = require('gulp')
var browserify = require('browserify')
var source = require('vinyl-source-stream')

var nodemon = require('nodemon')
var less = require('gulp-less')
var cssnano = require('gulp-cssnano')
var rename = require('gulp-rename')
var concat = require('gulp-concat')

var paths = {
  jsx: {
    source: 'app/index.jsx',
    watch: ['app/**/*.js*', 'config.js'],
    destination: 'public/js',
    name: 'app.min.js'
  },
  less: {
    source: 'less/style.less',
    watch: ['less/**/*.less'],
    destination: 'public/css',
    name: 'style.min.css'
  },
  vendor: {
    js: {
      sources: [
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js'
      ],
      destination: 'public/js',
      name: 'vendor.min.js'
    }
  }
}

gulp.task('install-vendor', function () {
  return gulp.src(paths.vendor.js.sources)
    .pipe(concat(paths.vendor.js.name))
    .pipe(gulp.dest(paths.vendor.js.destination))
})

gulp.task('jsx', function () {
  return browserify(paths.jsx.source)
    .transform('babelify', {presets: ['es2015', 'react']})
    .bundle()
    .pipe(source(paths.jsx.name))
    .pipe(gulp.dest(paths.jsx.destination))
})

gulp.task('less', function () {
  return gulp.src(paths.less.source)
    .pipe(less())
    .pipe(rename(paths.less.name))
    .pipe(gulp.dest(paths.less.destination))
})

gulp.task('build jsx', function () {
  return browserify(paths.jsx.source)
    .transform('babelify', {presets: ['es2015', 'react']})
    .transform({
      global: true
    }, 'uglifyify')
    .bundle()
    .pipe(source(paths.jsx.name))
    .pipe(gulp.dest(paths.jsx.destination))
})

gulp.task('build less', function () {
  return gulp.src(paths.less.source)
    .pipe(less())
    .pipe(cssnano())
    .pipe(rename(paths.less.name))
    .pipe(gulp.dest(paths.less.destination))
})

gulp.task('watch jsx', function () {
  return gulp.watch(paths.jsx.watch, ['jsx'])
})

gulp.task('watch less', function () {
  return gulp.watch(paths.less.watch, ['less'])
})

gulp.task('nodemon', function () {
  var monitor = nodemon({
    'script': 'index.js',
    'restartable': 'rs',
    'ignore': [
      '.git',
      'node_modules',
      'bower_components',
      'app',
      'public'
    ],
    'verbose': false,
    'execMap': {
      'js': 'node'
    },
    'watch': [
      'index.js',
      'dummy.js',
      'views/'
    ],
    'env': {
      'NODE_ENV': 'development',
      'DEBUG': 'almabase*'
    },
    'ext': 'js, hbs'
  })

  monitor.on('log', function (log) {
    debug(log.message)
  })

  process.once('SIGINT', function () {
    monitor.once('exit', function () {
      process.exit()
    })
  })
})

gulp.task('setup', ['install-vendor', 'build'])

gulp.task('watch', ['watch jsx', 'watch less'])
gulp.task('build', ['build jsx', 'build less'])
gulp.task('default', ['jsx', 'less', 'watch', 'nodemon'])
