var debug = require('debug')('almabase:index')

var path = require('path')
var express = require('express')
var compression = require('compression')
var hbs = require('hbs')
var bodyParser = require('body-parser')

var Dummy = require('./dummy')

var app = express()

app.set('view engine', 'hbs')
app.set('views', path.join(__dirname, 'views'))
hbs.registerPartials(path.join(__dirname, 'views/partials'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))
app.use(compression())

app.get('/', function (req, res) {
  res.render('index')
})

app.get('/api/section_and_fields', function (req, res) {
  res.json(Dummy.generate())
})

app.post('/api/new_field', function (req, res) {
  debug(req.body.sectionId, req.body.name, req.body.propertyName)
  res.json(Dummy.addAField(req.body.sectionId, req.body.name, req.body.propertyName))
})

app.post('/api/new_section', function (req, res) {
  debug(req.body.name, req.body.order)
  res.json(Dummy.addASection(req.body.name, req.body.order))
})

app.post('/api/edit_section', function (req, res) {
  res.json({
    id: req.body.id,
    name: req.body.name,
    aboveBelow: req.body.aboveBelow,
    targetSectionOrder: req.body.targetSectionOrder
  })
})

app.listen(process.env.PORT || 6060, function () {
  debug('Listening on port 6060...')
})
