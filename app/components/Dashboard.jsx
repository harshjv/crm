/* global $ */

var debug = require('debug')('almabase:jsx:dashboard')

var React = require('react')
var Sortable = require('react-anything-sortable')
var SortableContainer = Sortable.SortableContainer

var Section = require('./Section.jsx')
var SectionStore = require('../stores/SectionStore')

var ActionTypes = require('../constants/Constants').ActionTypes
var Dispatcher = require('../dispatcher/Dispatcher')
var Actions = require('../actions/Actions')

var Modal = require('../utils/Modal')

function getStateFromStore () {
  return {
    sections: SectionStore.getSections()
  }
}

var Dashboard = React.createClass({
  getInitialState: function () {
    return getStateFromStore()
  },

  componentDidMount: function () {
    SectionStore.addChangeListener(this._onChange)
  },

  componentWillUnmount: function () {
    SectionStore.removeChangeListener(this._onChange)
  },

  _onChange: function () {
    debug('SectionStore Changed')
    this.setState(getStateFromStore())
  },

  removeSection: function (id) {
    var action = {
      type: ActionTypes.REMOVE_SECTION,
      data: {
        id: id
      }
    }

    Dispatcher.dispatch(action)
  },

  onSortSections: function (sortedArray, currentDraggingSortData, currentDraggingIndex) {
    var action = {
      type: ActionTypes.REARRANGE_SECTION,
      data: {
        id: currentDraggingSortData,
        newOrder: currentDraggingIndex + 1
      }
    }

    Dispatcher.dispatch(action)
  },

  addASection: function () {
    var ref = this

    $('#aboveBelow').html('')
    $('#aboveBelowSection').html('')

    $.each({ 'Above': 0, 'Below': 1 }, function (key, value) {
      var t = $('<option></option>')
          .attr('value', value)
          .text(key)

      if (key === 'Above') {
        t.attr('selected', 'selected')
      }

      $('#aboveBelow').append(t)
    })

    $.each(ref.state.sections, function (key, value) {
      $('#aboveBelowSection').append($('<option></option>')
          .attr('value', value.order)
          .text(value.name))
    })

    $('#add-a-section-title').text('Add a section')

    Modal('#add-a-section', function (event, modal) {
      modal.find('#add-section-name').val('')
      modal.find('button.btn-primary').text('Add')
    }, function (event, modal) {
      $('#add-section-name').focus()
      modal.find('button.btn-primary').on('click', function () {
        var sectionName = modal.find('#add-section-name').val()
        var aboveBelow = parseInt(modal.find('#aboveBelow').val())
        var targetSectionOrder = parseInt(modal.find('#aboveBelowSection').val())

        if (sectionName === '') {
          window.alert('Blank names are not allowed.')
        } else if (SectionStore.checkIfExist(sectionName)) {
          window.alert(`A section with name ${sectionName} already exists.`)
        } else {
          $(this).unbind('click')
          Actions.newSection(sectionName, targetSectionOrder + aboveBelow)
          modal.modal('hide')
        }
      })
    })
  },

  editSection: function (sectionId) {
    var ref = this
    var selectedSection = SectionStore.getSection(sectionId)

    $('#aboveBelow').html('')
    $('#aboveBelowSection').html('')

    $.each({ 'Above': 'above', 'Below': 'below' }, function (key, value) {
      var t = $('<option></option>')
          .attr('value', value)
          .text(key)

      if (selectedSection.order === ref.state.sections.length) {
        if (key === 'Below') {
          t.attr('selected', 'selected')
        }
      } else {
        if (key === 'Above') {
          t.attr('selected', 'selected')
        }
      }

      $('#aboveBelow').append(t)
    })

    $.each(ref.state.sections, function (key, value) {
      if (value.id === sectionId) return

      var t = $('<option></option>')
          .attr('value', value.order)
          .text(value.name)

      if (selectedSection.order === ref.state.sections.length) {
        if (value.order === ref.state.sections.length - 1) {
          t.attr('selected', 'selected')
        }
      } else if (selectedSection.order + 1 === value.order) {
        t.attr('selected', 'selected')
      }

      $('#aboveBelowSection').append(t)
    })

    $('#add-a-section-title').text('Edit section')

    Modal('#add-a-section', function (event, modal) {
      modal.find('#add-section-name').val(selectedSection.name)
      modal.find('button.btn-primary').text('Edit')
    }, function (event, modal) {
      $('#add-section-name').focus()
      modal.find('button.btn-primary').on('click', function () {
        var sectionName = modal.find('#add-section-name').val()
        var aboveBelow = modal.find('#aboveBelow').val()
        var targetSectionOrder = parseInt(modal.find('#aboveBelowSection').val())

        if (sectionName === '') {
          window.alert('Blank names are not allowed.')
        } else if (SectionStore.isValidEditing(selectedSection.name, sectionName)) {
          window.alert(`A section with name ${sectionName} already exists.`)
        } else {
          $(this).unbind('click')
          Actions.editSection(selectedSection.id, sectionName, aboveBelow, targetSectionOrder)
          modal.modal('hide')
        }
      })
    })
  },

  getSections: function () {
    return this.state.sections.map(function (section, index) {
      return (
        <SortableContainer className='container section' sortData={section.id} key={section.id}>
          <div>
            <Section id={section.id} name={section.name} onRemoveSection={this.removeSection} onEditSection={this.editSection} />
          </div>
        </SortableContainer>
      )
    }, this)
  },

  render: function () {
    debug('Rendering Dashboard')

    var r

    if (this.state.sections.length === 0) {
      r = (
        <h3 className='text-center text-muted nfe'>No Sections Exist</h3>
      )
    } else {
      r = (
        <Sortable sortHandle='sort-sections' onSort={this.onSortSections} dynamic>
          {this.getSections()}
        </Sortable>
      )
    }

    return (
      <div className='container'>
        <div className='page-header'>
          <button className='btn btn-primary pull-right' onClick={this.addASection}>
            Add new section
          </button>
          <h2>Profile View</h2>
        </div>
        {r}
      </div>
    )
  }
})

module.exports = Dashboard
