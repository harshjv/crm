// var debug = require('debug')('almabase:jsx:field')

var React = require('react')

var Field = React.createClass({
  propTypes: {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired
  },

  handleRemove: function () {
    if (window.confirm(`You really want to remove "${this.props.name}" field?`)) {
      this.props.onRemoveField(this.props.id)
    }
  },

  render: function () {
    return (
      <div className='field-name'>
        {this.props.name}
        <span className='pull-right'>
          <i className='fa fa-arrows-alt'></i>
          <i className='fa fa-times text-pointer' onClick={this.handleRemove}></i>
        </span>
      </div>
    )
  }
})

module.exports = Field
