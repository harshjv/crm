var React = require('react')

var Navbar = React.createClass({
  propTypes: {
    user: React.PropTypes.object.isRequired
  },

  render: function () {
    return (
      <nav className='navbar navbar-default navbar-fixed-top'>
        <div className='container'>
          <div className='navbar-header'>
            <button
              type='button'
              className='navbar-toggle collapsed'
              data-toggle='collapse'
              data-target='#almabase-dashboard-navbar'
              aria-expanded='false'>
              <span className='sr-only'>Toggle navigation</span>
              <span className='icon-bar'></span>
              <span className='icon-bar'></span>
              <span className='icon-bar'></span>
            </button>
            <a className='navbar-brand' href='/Dashboard'>Dopplr</a>
          </div>
          <div className='collapse navbar-collapse' id='almabase-dashboard-navbar'>
            <ul className='nav navbar-nav navbar-right'>
              <li><a>{this.props.user.fullName} <span className='badge'>{this.props.user.username}</span></a></li>
              <li><a href='/Logout'>Log out</a></li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
})

module.exports = Navbar
