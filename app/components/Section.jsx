/* global $ */

var debug = require('debug')('almabase:jsx:section')

var React = require('react')
var Sortable = require('react-anything-sortable')
var SortableContainer = Sortable.SortableContainer

var Field = require('./Field.jsx')
var FieldStore = require('../stores/FieldStore')

var ActionTypes = require('../constants/Constants').ActionTypes
var Dispatcher = require('../dispatcher/Dispatcher')
var Actions = require('../actions/Actions')

var Modal = require('../utils/Modal')

function getStateFromStore (sectionId) {
  debug(FieldStore.getFields(sectionId))
  return {
    fields: FieldStore.getFields(sectionId)
  }
}

var Section = React.createClass({
  propTypes: {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired
  },

  getInitialState: function () {
    return getStateFromStore(this.props.id)
  },

  componentDidMount: function () {
    FieldStore.addChangeListener(this._onChange)
  },

  componentWillUnmount: function () {
    FieldStore.removeChangeListener(this._onChange)
  },

  _onChange: function () {
    debug('FieldStore Changed')
    this.setState(getStateFromStore(this.props.id))
  },

  addAField: function () {
    var ref = this

    Modal('#add-a-field', function (event, modal) {
      modal.find('#add-field-name').val('')
      modal.find('#add-a-field-section-name').text(ref.props.name)
    }, function (event, modal) {
      modal.find('#add-field-name').focus()
      modal.find('button.btn-primary').on('click', function () {
        var fieldName = modal.find('#add-field-name').val()

        if (fieldName === '') {
          window.alert('Blank names are not allowed.')
        } else if (FieldStore.checkIfExist(fieldName)) {
          window.alert(`A section with name ${fieldName} already exists.`)
        } else {
          $(this).unbind('click')
          Actions.newField(ref.props.id, fieldName, 'Dummy')
          modal.modal('hide')
        }
      })
    })
  },

  removeField: function (id) {
    var action = {
      type: ActionTypes.REMOVE_FIELD,
      data: {
        id: id,
        sectionId: this.props.id
      }
    }

    Dispatcher.dispatch(action)
  },

  handleRemove: function () {
    if (window.confirm(`You really want to remove "${this.props.name}" section?`)) {
      this.props.onRemoveSection(this.props.id)
    }
  },

  handleEdit: function () {
    this.props.onEditSection(this.props.id)
  },

  getFields: function () {
    return this.state.fields.map(function (field, index) {
      return (
        <SortableContainer className='col-lg-3 field' sortData={field} key={field.id}>
          <div>
            <Field id={field.id} name={field.name} onRemoveField={this.removeField} />
          </div>
        </SortableContainer>
      )
    }, this)
  },

  onSortFields: function (sortedArray, currentDraggingSortData, currentDraggingIndex) {
    var action = {
      type: ActionTypes.REARRANGE_FIELD,
      data: {
        id: currentDraggingSortData.id,
        newOrder: currentDraggingIndex + 1
      }
    }

    Dispatcher.dispatch(action)
  },

  render: function () {
    debug('Rendering Section: ' + this.props.name)

    var r

    if (this.state.fields.length === 0) {
      r = (
        <h3 className='text-center text-muted nfe'>No Fields Exist</h3>
      )
    } else {
      r = (
        <Sortable className='row' sortHandle='fa-arrows-alt' onSort={this.onSortFields} dynamic>
          {this.getFields()}
        </Sortable>
      )
    }

    return (
      <div>
        <div className='section-title'>
          <h4>{this.props.name}</h4>
          <h5 onClick={this.handleEdit} className='text-pointer'>Edit section</h5>
          <h5 onClick={this.handleRemove} className='text-pointer'>Delete section</h5>
          <h5 className='sort-sections'>Move section</h5>
          <h5 className='pull-right text-primary text-pointer' onClick={this.addAField}>Add a field</h5>
        </div>

        {r}
      </div>
    )
  }
})

module.exports = Section
