var debug = require('debug')('almabase:store:section')

var EventEmitter = require('events').EventEmitter

var Store = require('./Store')
var Constants = require('../constants/Constants')
var Dispatcher = require('../dispatcher/Dispatcher')

var assign = require('object-assign')

var ActionTypes = Constants.ActionTypes

var _sections = {}

var SectionStore = assign({}, Store, EventEmitter.prototype, {
  getSections: function () {
    var sections = []

    for (var id in _sections) {
      sections.push(_sections[id])
    }

    sections.sort(function (a, b) {
      return a.order - b.order
    })

    return sections
  },

  getSection: function (id) {
    debug('getSection ' + id)
    return _sections[id]
  },

  setFieldCount: function (id, count) {
    _sections[id].fieldCount = count
  },

  getFieldCount: function (id) {
    return _sections[id].fieldCount
  },

  checkIfExist: function (name) {
    for (var id in _sections) {
      if (_sections[id].name.toLowerCase() === name.toLowerCase()) return true
    }

    return false
  },

  isValidEditing: function (oldName, newName) {
    for (var id in _sections) {
      if (_sections[id].name === oldName) continue

      if (_sections[id].name.toLowerCase() === newName.toLowerCase()) return true
    }

    return false
  }
})

function register (action) {
  return {
    [ ActionTypes.INIT ]: function () {
      debug('INIT')

      for (var section of action.data.sections) {
        _sections[section.id] = section
      }

      debug('INIT:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.REMOVE_SECTION ]: function () {
      debug('REMOVE_SECTION')

      var oldOrder = _sections[action.data.id].order

      delete _sections[action.data.id]

      for (var fieldId in _sections) {
        if (_sections[fieldId].order > oldOrder) _sections[fieldId].order--
      }

      debug('REMOVE_SECTION:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.REARRANGE_SECTION ]: function () {
      debug('REARRANGE_SECTION')
      var currentSection = _sections[action.data.id]
      var oldOrder = currentSection.order
      var newOrder = action.data.newOrder
      var goingUp = newOrder > oldOrder

      for (var sectionId in _sections) {
        var currentOrder = _sections[sectionId].order

        if (goingUp && currentOrder <= newOrder && currentOrder > oldOrder) {
          _sections[sectionId].order--
        } else if (currentOrder >= newOrder && currentOrder < oldOrder) {
          _sections[sectionId].order++
        }
      }

      _sections[action.data.id].order = action.data.newOrder

      debug('REARRANGE_SECTION:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.EDIT_SECTION ]: function () {
      debug('EDIT_SECTION')
      var currentSection = _sections[action.data.id]
      var oldOrder = currentSection.order
      var aboveBelow = action.data.aboveBelow
      var targetSectionOrder = action.data.targetSectionOrder

      var newOrder

      if (oldOrder - targetSectionOrder < 0) {
        if (aboveBelow === 'below') {
          newOrder = targetSectionOrder
        } else {
          newOrder = targetSectionOrder - 1
        }
      } else {
        if (aboveBelow === 'below') {
          newOrder = targetSectionOrder + 1
        } else {
          newOrder = targetSectionOrder
        }
      }

      var goingUp = newOrder > oldOrder

      for (var sectionId in _sections) {
        var currentOrder = _sections[sectionId].order

        if (goingUp && currentOrder <= newOrder && currentOrder > oldOrder) {
          _sections[sectionId].order--
        } else if (currentOrder >= newOrder && currentOrder < oldOrder) {
          _sections[sectionId].order++
        }
      }

      _sections[action.data.id].order = newOrder
      _sections[action.data.id].name = action.data.name

      debug('EDIT_SECTION:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.NEW_SECTION ]: function () {
      debug('NEW_SECTION')

      var newOrder = action.data.order

      for (var sectionId in _sections) {
        if (_sections[sectionId].order >= newOrder) _sections[sectionId].order++
      }

      _sections[action.data.id] = action.data

      debug('NEW_SECTION:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.NEW_FIELD ]: function () {
      debug('NEW_FIELD')
      debug(action.data)

      _sections[action.data.sectionId].fieldCount++

      debug('NEW_FIELD:EMIT_CHANGE')
      SectionStore.emitChange()
    },

    [ ActionTypes.REMOVE_FIELD ]: function () {
      debug('REMOVE_FIELD')
      debug(action.data)

      _sections[action.data.sectionId].fieldCount--

      debug('REMOVE_FIELD:EMIT_CHANGE')
      SectionStore.emitChange()
    }
  }
}

SectionStore.dispatchToken = Dispatcher.register(function (action) {
  (register(action)[action.type] || function () {})()
})

module.exports = SectionStore
