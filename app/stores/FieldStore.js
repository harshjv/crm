var debug = require('debug')('almabase:store:field')

var EventEmitter = require('events').EventEmitter

var Store = require('./Store')

var Constants = require('../constants/Constants')
var Dispatcher = require('../dispatcher/Dispatcher')
var SectionStore = require('./SectionStore')

var assign = require('object-assign')

var ActionTypes = Constants.ActionTypes

var _fields = {}

var FieldStore = assign({}, Store, EventEmitter.prototype, {
  getFields: function (sectionId) {
    var fields = []

    if (Object.keys(_fields).length === 0) return []

    for (var fieldId in _fields) {
      if (_fields[fieldId].sectionId === sectionId) fields.push(_fields[fieldId])
    }

    fields.sort(function (a, b) {
      return a.order - b.order
    })

    return fields
  },

  getField: function (id) {
    return _fields[id]
  },

  checkIfExist: function (name) {
    for (var id in _fields) {
      if (_fields[id].name.toLowerCase() === name.toLowerCase()) return true
    }

    return false
  }
})

function register (action) {
  return {
    [ ActionTypes.INIT ]: function () {
      debug('Wait for SectionStore')
      Dispatcher.waitFor([SectionStore.dispatchToken])
      debug('SectionStore Updated')
      debug('INIT')

      for (var field of action.data.fields) {
        _fields[field.id] = field
      }

      debug('INIT:EMIT_CHANGE')
      FieldStore.emitChange()
    },

    [ ActionTypes.REMOVE_FIELD ]: function () {
      debug('Wait for SectionStore')
      Dispatcher.waitFor([SectionStore.dispatchToken])
      debug('SectionStore Updated')

      debug('REMOVE_FIELD')

      var oldOrder = _fields[action.data.id].order
      var sectionId = _fields[action.data.id].sectionId

      delete _fields[action.data.id]

      for (var fieldId in _fields) {
        if (_fields[fieldId].sectionId !== sectionId) continue
        if (_fields[fieldId].order > oldOrder) _fields[fieldId].order--
      }

      debug('REMOVE_FIELD:EMIT_CHANGE')
      FieldStore.emitChange()
    },

    [ ActionTypes.REARRANGE_FIELD ]: function () {
      debug('REARRANGE_FIELD')
      var currentField = _fields[action.data.id]
      var sectionId = currentField.sectionId
      var oldOrder = currentField.order
      var newOrder = action.data.newOrder
      var goingUp = newOrder > oldOrder

      for (var fieldId in _fields) {
        if (_fields[fieldId].sectionId !== sectionId) continue

        var currentOrder = _fields[fieldId].order

        if (goingUp && currentOrder <= newOrder && currentOrder > oldOrder) {
          _fields[fieldId].order--
        } else if (currentOrder >= newOrder && currentOrder < oldOrder) {
          _fields[fieldId].order++
        }
      }

      _fields[action.data.id].order = action.data.newOrder

      debug('REARRANGE_FIELD:EMIT_CHANGE')
      FieldStore.emitChange()
    },

    [ ActionTypes.NEW_FIELD ]: function () {
      debug('Wait for SectionStore')
      Dispatcher.waitFor([SectionStore.dispatchToken])
      debug('SectionStore Updated')

      debug('NEW_FIELD')

      _fields[action.data.id] = {
        id: action.data.id,
        sectionId: action.data.sectionId,
        name: action.data.name,
        order: action.data.order || SectionStore.getFieldCount(action.data.sectionId)
      }

      debug('NEW_FIELD:EMIT_CHANGE')
      FieldStore.emitChange()
    }
  }
}

FieldStore.dispatchToken = Dispatcher.register(function (action) {
  (register(action)[action.type] || function () {})()
})

module.exports = FieldStore
