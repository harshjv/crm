/* global $ */

function Modal (modalId, beforeShow, onShow) {
  $(modalId).one('show.bs.modal', function (event) {
    beforeShow(event, $(this))
  })

  $(modalId).one('shown.bs.modal', function (event) {
    onShow(event, $(this))
  })

  $(modalId).one('hide.bs.modal', function (event) {
    $(this).find('button.btn-primary').unbind('click')
  })

  $(modalId).modal('show')
}

module.exports = Modal
