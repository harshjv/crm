var axios = require('axios')

var instance = axios.create({
  baseURL: '/api/',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Expires': '-1',
    'Cache-Control': 'no-cache,no-store,must-revalidate,max-age=-1,private'
  }
})

module.exports = instance
