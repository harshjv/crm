var keyMirror = require('keymirror')

module.exports = {
  ActionTypes: keyMirror({
    // SECTION
    INIT: null,

    INIT_SECTIONS: null,
    NEW_SECTION: null,
    REMOVE_SECTION: null,
    REARRANGE_SECTION: null,

    // FIELD
    INIT_FIELDS: null,
    NEW_FIELD: null,
    REMOVE_FIELD: null,
    REARRANGE_FIELD: null,

    // OTHER
    ERROR: null
  })
}
