var debug = require('debug')('almabase:actions')

var Constants = require('../constants/Constants')
var Dispatcher = require('../dispatcher/Dispatcher')

var request = require('../utils/RequestUtils')

var ActionTypes = Constants.ActionTypes

var dispatchError = function () {
  var action = {
    type: ActionTypes.ERROR
  }

  Dispatcher.dispatch(action)
}

var Actions = {
  initialize: function () {
    this.getData()
  },

  getData: function () {
    debug('Getting data')
    request.get('/section_and_fields')
    .then(function (response) {
      if (response.status === 200) {
        var action = {
          type: ActionTypes.INIT,
          data: {
            sections: response.data.sections,
            fields: response.data.fields
          }
        }

        Dispatcher.dispatch(action)
      } else {
        dispatchError()
      }
    }).catch(function (response) {
      dispatchError()
    })
  },

  newField: function (sectionId, fieldName, propertyName) {
    debug('Adding a new field')
    request.post('/new_field', { sectionId: sectionId, name: fieldName, propertyName: propertyName })
    .then(function (response) {
      if (response.status === 200) {
        var action = {
          type: ActionTypes.NEW_FIELD,
          data: {
            id: response.data.id,
            sectionId: response.data.sectionId,
            name: response.data.name,
            order: response.data.order
          }
        }

        Dispatcher.dispatch(action)
      } else {
        dispatchError()
      }
    }).catch(function (response) {
      dispatchError()
    })
  },

  newSection: function (sectionName, order) {
    debug('Adding a new section')
    request.post('/new_section', { name: sectionName, order: order })
    .then(function (response) {
      if (response.status === 200) {
        var action = {
          type: ActionTypes.NEW_SECTION,
          data: {
            id: response.data.id,
            name: response.data.name,
            order: response.data.order,
            fieldCount: response.data.fieldCount
          }
        }

        Dispatcher.dispatch(action)
      } else {
        dispatchError()
      }
    }).catch(function (response) {
      dispatchError()
    })
  },

  editSection: function (id, sectionName, aboveBelow, targetSectionOrder) {
    debug('Edit a new section')
    request.post('/edit_section', { id: id, name: sectionName, aboveBelow: aboveBelow, targetSectionOrder: targetSectionOrder })
    .then(function (response) {
      if (response.status === 200) {
        var action = {
          type: ActionTypes.EDIT_SECTION,
          data: {
            id: response.data.id,
            name: response.data.name,
            aboveBelow: aboveBelow,
            targetSectionOrder: targetSectionOrder
          }
        }

        Dispatcher.dispatch(action)
      } else {
        dispatchError()
      }
    }).catch(function (response) {
      dispatchError()
    })
  }
}

module.exports = Actions
