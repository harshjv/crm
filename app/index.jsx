var React = require('react')
var ReactDOM = require('react-dom')

var Dashboard = require('./components/Dashboard.jsx')
var Actions = require('./actions/Actions')

Actions.initialize()
ReactDOM.render(<Dashboard />, document.getElementById('main'))
