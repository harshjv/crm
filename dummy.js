var uuid = require('node-uuid')

var _sections = [
  'Biodata',
  'Education',
  'Personal Details'
]

var _fields = [
  'Full Name',
  'Date of Birth',
  'Father\'s name',
  'Languages',
  'Contact Number',
  'Email ID',
  'Contact Address',
  'Facebook ID'
]

module.exports.addAField = function (sectionId, fieldName, propertyName) {
  return {
    id: uuid.v4(),
    sectionId: sectionId,
    name: fieldName
  }
}

module.exports.addASection = function (sectionName, order) {
  return {
    id: uuid.v4(),
    name: sectionName,
    order: order,
    fieldCount: 0
  }
}

module.exports.generate = function () {
  var sections = []
  var fields = []
  var i = 1

  for (var section of _sections) {
    var j = 1

    var s = {
      id: uuid.v4(),
      name: section,
      order: i++,
      fieldCount: _fields.length
    }

    sections.push(s)

    for (var field of _fields) {
      var f = {
        id: uuid.v4(),
        sectionId: s.id,
        name: field,
        order: j++
      }

      fields.push(f)
    }
  }

  return {
    sections: sections,
    fields: fields
  }
}
